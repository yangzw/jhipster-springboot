/**
 * View Models used by Spring MVC REST controllers.
 */
package com.juzi.web.rest.vm;
